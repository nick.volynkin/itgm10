.. Docs like code documentation master file, created by
   sphinx-quickstart on Mon May  8 23:07:21 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Docs like code's documentation!
==========================================

..  revealjs::

    ..  revealjs:: Docs like code

        Работаем с документацией, как с программным кодом.

    .. (Пролистывайте слайды **вниз**, потом влево с помощью стрелок на клавиатуре)

    ..  revealjs::

        Николай Волынкин

        * Технический писатель, `Plesk`_.
        * Модератор сообщества `Stack Overflow на русском`_
        * Ранее: автоматизация тестирования, 2GIS
        * Ещё ранее: разработка мобильных приложений

        .. _Plesk: https://www.plesk.com/
        .. _Stack Overflow на русском: https://ru.stackoverflow.com

    ..  revealjs:: Ссылки

        Упоминаемые языки, инструменты, другие ресурсы будут сопровождаться ссылками.

        Презентация всегда будет доступна по адресу

        `nick.volynkin.gitlab.io/itgm10`_

        .. _nick.volynkin.gitlab.io/itgm10: http://nick.volynkin.gitlab.io/itgm10/

    ..  revealjs:: Термины

        Все вводимые термины будут продублированы на английском языке, потому что литературы на русском почти нет.

    ..  revealjs:: Вопросы

        Вопросы — в конце лекции.

        Если вы не успеете задать вопрос или придумаете его после лекции:

        nick.volynkin@gmail.com


..  revealjs::

    ..  revealjs:: Что мы обсудим

        Тема огромная, постараемся успеть понемногу.

        #.  Что такое «документация как код».
        #.  Что может быть кодом (почти всё).
        #.  Возможности и ограничения подхода.
        #.  Обзор инструментов.
        #.  С чего начать.

    ..  revealjs:: Что хотелось бы обсудить

        но не успеем точно.

        *   Совместная работа над документацией,
        *   Локализация,
        *   Полнотекстовый поиск,
        *   Agile,
        *   Тестирование документации,
        *   Crowdsourcing.

    ..  revealjs:: Аудитория

        Здесь «мы» — это все, причастные к разработке документации:

        *   Технические писатели,
        *   Аналитики,
        *   Тестировщики,
        *   Разработчики ПО,
        *   Руководители,
        *   Неравнодушные читатели,
        *   Любой, у кого душа болит о документации.

..  revealjs::

    ..  revealjs:: Смысл и суть

        Docs like/as code (документация в форме кода) — это подход к разработке документации, в котором мы:

        *   Разрабатываем документацию как программный код: пишем в текстовом редакторе, а потом «компилируем» в конечный продукт.
        *   Используем лучшие практики и инструменты, придуманные для разработки ПО.
        *   Отдаём машине все рутинные задачи и занимаемся любимым делом.

        Как DevOps, только про документацию.

    ..  revealjs:: Этим кто-то пользуется?

        ..  rst-class:: fragment

            *   Linux Kernel
            *   Microsoft
            *   Openstack
            *   Ansible
            *   GitLab

..  revealjs::

    ..  revealjs:: Разработка документации

        ..  raw:: html

            <ol>
            <li class="fragment">Содержимое (content)</li>
            <li class="fragment">Представление (representation)</li>
            <li class="fragment">Сборка (building)</li>
            <li class="fragment">Хранение и версионирование (storage, version control)</li>
            <li class="fragment">Публикация (publishing)</li>
            <li class="fragment">Рабочий процесс (workflow)</li>
            <li class="fragment">Руководства и требования (guides and requirements)</li>
            </ol>

..  revealjs::

    ..  revealjs:: Содержимое

        Текст, изображения, видео, скачиваемые файлы.

        Структура: абзацы, заголовки, документы, книги.

    ..  revealjs:: Код

        Легковесные языки разметки (lightweight markup languages).

        Пишем структурированный текст, который:

        *   Легко читается человеком без специального ПО.
        *   Имеет структурную и семантическую разметку и может однозначно обрабатываться машиной.

    ..  revealjs:: Редактор

        Для работы достаточно текстового редактора.

        В хороших редакторах есть подсветка синтаксиса и предпросмотр результата.

    ..  revealjs:: Популярные языки:

        ..  list-table::
            :header-rows: 0
            :align: left

            *   -   Общие:
                -   -   Markdown
                    -   reStructured Text
                    -   AsciiDoc
            *   -   Специальные:
                -   -   Для документирования API (OpenAPI)
                    -   Коментарии к коду (Doxygen)
                    -   Особого назначения (LaTeX)

    ..  revealjs:: Заголовок

        Markdown:

        ..  code-block:: md

            # Header 1

            Text...

        reStructured Text:

        ..  code-block:: rst

            Header 1
            ========

            Text...

    ..  revealjs:: Ссылка

        Markdown:

        ..  code-block:: md

            Hello, [ITGM 10](http://piter-united.ru/itgm10/)!

        reStructured Text:

        ..  code-block:: rst

            Hello, `ITGM 10`_!

            .. _`ITGM 10`: http://piter-united.ru/itgm10/

        Результат:

        Hello, `ITGM 10`_!

        .. _`ITGM 10`: http://piter-united.ru/itgm10/

    ..  revealjs:: Фрагмент кода



        Markdown:

        ..  code-block:: md

            ```bash
            echo 'hello world'
            ```

        reStructured Text:

        ..  code-block:: rst

            ..  code-block:: bash

                echo 'Hello, ITGM10!'

        Результат:

        ..  code-block:: bash

            echo 'Hello, ITGM10!'

    ..  revealjs:: Оглавление

        Markdown

        Просто делаем список со ссылками:

        ..  code-block:: md

            # Contents

            * [Chapter 1](/chapter-1)
                * [Paragraph 1](/chapter-1/#paragraph-1)
            * [Chapter 2](/chapter-2)
            * [Chapter 3](/chapter-3)

        reStructured Text

        Описываем структуру документа:

        ..  code-block:: rst

            ..  toctree::
                :maxdepth: 2
                :caption: Contents

                chapter1
                chapter2
                chapter3

..  revealjs::

    ..  revealjs:: Сборка документации

        Получаем комплект документации, который можно отдавать:

        * На рецензирование (проверку, ревью)
        * На приёмку
        * Читателю (заказчику)

    ..  revealjs:: Сборка документации из кода

        Используем *фреймворки документации* (documentation frameworks).

        Это инструменты, которые преобразуют исходный код в форматы для публикации.


    ..  revealjs:: Представление

        В процессе сборки создаётся представление документа — то, как читатель видит и может использовать документацию.

        *   Шрифты и стили оформления текста
        *   Видимая структура и навигация по документам
        *   Пользовательский опыт (user experience)

    ..  revealjs:: Представление


        Содержимое и представление отделены друг от друга. Для одного содержимого может быть несколько представлений (например, сайт, PDF, печать).

        Да, это тот самый *принцип единого источника* (single source).
        Он же в программировании – Single Point of Truth (SPOT) и Don't Repeat Yourself (DRY).

    ..  revealjs:: Темы

        *Тема* (theme) — набор стилей и шаблонов, определяющих представление конечного формата.

        Как правило, тема предназначена для конкретного фреймворка.

        Готовых тем множество, можно редактировать или создавать темы для своих задач.
        (Понадобится фронтенд-разработчик).

        Вообще всю настройку сборки можно делегировать разработчику.

    ..  revealjs:: Фреймворки

        (некоторые наиболее популярные)

        ..  list-table::
            :header-rows: 1

            *   -   Фреймворк:
                -   MD
                -   rST
                -   AsciiDoc
            *   -   `Jekyll`_
                -   ``+``
                -   ``-``
                -   ``-``
            *   -   `Sphinx`_
                -   ``-``
                -   ``+``
                -   ``-``
            *   -   `AsciiDoctor`_
                -   ``-``
                -   ``-``
                -   ``+``
            *   -   `Hugo`_
                -   ``+``
                -   ``+/-``
                -   ``+/-``
            *   -   `Hexo`_
                -   ``+``
                -   ``-``
                -   ``-``
            *   -   `Nikola`_
                -   ``+``
                -   ``+``
                -   ``-``

    ..  _Sphinx: http://www.sphinx-doc.org/
    ..  _Hugo: https://gohugo.io/
    ..  _Jekyll: https://jekyllrb.com/
    ..  _Hexo: https://hexo.io/
    ..  _Nikola: https://getnikola.com/
    ..  _AsciiDoctor: http://asciidoctor.org/


    ..  revealjs:: Pandoc

        `Pandoc`_ – швейцарский нож техписателя.

        Конвертирует из ~20 форматов в ~40 форматов.

    ..  _Pandoc: http://pandoc.org/

..  revealjs::

    ..  revealjs:: Drink Your Own Champagne

        Эта презентация — тоже объект документации.

    ..  revealjs:: Разработка

        *   Исходный код в разметке reStructured Text (rST).
        *   Из исходного кода в rST помощью инструмента Sphinx и темы `sphinxjp.themes.revealjs`_
            собирается код сайта (HTML, CSS и Javascript).

        *   Этот код основан на специальном фреймворке для презентаций — `reveal.js`_.

        Все использованные инструменты бесплатны. Знать HTML, CSS и Javascript не нужно.


    ..  revealjs:: Разработка: процесс

        .. image:: _static/workflow.png

    ..  revealjs:: Разработка: процесс

        .. image:: _static/workflow2.png

    ..  revealjs:: Представление

        Давайте «поиграем шрифтами».

        Происходит переключение между таблицами стилей (``.css``) с разными темами оформления.

        ..  raw:: html

            <!-- Hacks to swap themes after the page has loaded. Not flexible and only intended for the reveal.js demo deck. -->
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/black.css'); return false;">Black</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/white.css'); return false;">White</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/league.css'); return false;">League</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/sky.css'); return false;">Sky</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/beige.css'); return false;">Beige</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/simple.css'); return false;">Simple</a> <br>
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/serif.css'); return false;">Serif (default)</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/night.css'); return false;">Night</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/moon.css'); return false;">Moon</a> -
            <a href="#" onclick="document.getElementById('theme').setAttribute('href','_static/css/theme/solarized.css'); return false;">Solarized</a>

..  revealjs::

    ..  revealjs:: Хранение и контроль версий

        ..  rst-class:: fragment

            Ура, мы можем хранить наш код в системе контроля версий!

            * Git
            * И другие

    ..  revealjs:: Контроль версий

        *   Можем связать версию продукта и версию документации.
        *   Можем сохранять черновики или копить обновления к релизу.
        *   Не боимся всё потерять, если сервер сгорит.

    ..  revealjs:: Diffability

        Можем сравнить две любые версии вплоть до пробела или запятой.

        .. code-block:: bash

            git diff

        .. image:: _static/diffability.png

    ..  revealjs:: Blameability

        Достоверно знаем, кто, когда и **зачем** редактировал каждую строку.

        ..  code-block:: bash

            git blame


    ..  revealjs::

        .. image:: _static/blame.png

..  revealjs::

    ..  revealjs:: Публикация

        Как мы передаём документацию читателю.

    ..  revealjs:: Настало время автоматизации

        Все составляющие доступны машине:

        *   Исходный код лежит в системе контроля версий
        *   Фреймворки и темы оформления описываются как зависимости в файлах конфигурации
        *   Что и как собирать — инструкция в соседнем файле.

        Можно собирать и публиковать документацию. Кто будет это делать?

        Можно руками, но лучше...

    ..  revealjs:: Сервер непрерывной интеграции

        Continuous integration (CI) server.

        Кратко: машина, способная самостоятельно и по запросу выполнять заранее запрограммированные задачи.

        Дальше — DevOps. Делегируйте эту задачу системным администраторам.

.. revealjs::

    ..  revealjs:: Drink Your Own Champagne

    ..  revealjs:: Хранение, сборка и публикация

        *   Код хранится в локальном git-репозитории, оттуда отправляется на GitLab.com
        *   Сборка и публикация происходит на сервере непрерывной интеграции `GitLab CI`_.
            Используются серверные мощности из облака, предоставленного `DigitalOcean`_.
        *   Сайт хостится с помощью сервиса GitLab Pages на `nick.volynkin.gitlab.io/itgm10`_.

        Вся задействованная инфраструктура тоже бесплатна.
        Навыки системного администратора не требуются.

        .. _reveal.js: http://lab.hakim.se/reveal-js/#/
        .. _sphinxjp.themes.revealjs: https://github.com/tell-k/sphinxjp.themes.revealjs
        .. _DigitalOcean: https://www.digitalocean.com/
        .. _GitLab CI: https://about.gitlab.com/features/gitlab-ci-cd/

    ..  revealjs:: Локальная сборка

        Сборка сайта на рабочей машине (2 секунды):

        ..  code-block:: console

            make html

        Можно автоматизировать сборку по каждому изменению исходного кода, но пока руки не дошли.

        .. image:: _static/build.png

    ..  revealjs:: Сборка и публикация

        Подготовка окружения, сборка и публикация на сервере CI (~2 минуты):

        ..  code-block:: yaml

            image: alpine

            pages:
              script:
              - apk --no-cache add py2-pip python-dev
              - pip install -r requirements.txt
              - apk --no-cache add make
              - make html
              - mv build/html/ public/
              artifacts:
                paths:
                - public
              only:
              - master

    ..  revealjs:: Сборочные конвейеры на GitLab CI

        ..  image:: _static/pipelines.png

..  revealjs::

    ..  revealjs:: Рабочий процесс

        *   Как мы ставим задачи на разработку документации?
        *   Как мы фиксируем ошибки?
        *   Как происходит рецензирование документа?
        *   Как происходит приёмка документа?

    ..  revealjs:: Задачи на разработку и ошибки

        Тот же трекер задач, почти ничего нового.

    ..  revealjs:: Рецензирование и приёмка

        Используем функциональность пулл-реквестов (pull-request).

        Это запрос на вливание ваших изменений в основную ветку разработки.

        *   Видны изменения исходного кода, можно обсуждать каждую строку.
        *   Можно обсуждать запрос в целом.
        *   Право принять изменения обычно есть у того, кто принимает решение о приёмке.
        *   Если настроить — виден готовый к публикации результат.
            Например, документация публикуется на тестовом сайте.

    ..  revealjs:: Пример

        ..  image:: _static/pull-request.png

..  revealjs::

    ..  revealjs:: Стандарты и требования

        Все общие требования, которые мы предъявляем к документации:

        *   Оформление (заголовки, ссылки, библиография, отступы)
        *   Язык (грамотность, словарь, синтаксис)
        *   Актуальность и версионируемость
        *   Корректность и полнота


    ..  revealjs::   Оформление

        Представление задано в одном месте (single point of truth) и применяется автоматически ко всему документу.

        Когда меняются требования к оформлению, мы редактируем тему.

        С ГОСТами сложнее. Но если один раз реализовать нужную тему, она будет работать.

    ..  revealjs::   Язык

        Богатые возможности.

        *   Есть инструменты для проверки орфографии и пунктуации.
        *   Можно реализовать проверку словаря.
        *   Если вы умеете Natural Language Processing, возможностей становится больше.

        Ограничение: такие инструменты потребуют точной настройки под ваш проект,
        но всё равно будут иногда создавать ложноотрицательные результаты тестов.

    ..  revealjs::   Актуальность и версионируемость

        Поскольку мы используем один инструментарий с разработчиками, мы можем синхронизировать версии документации и версии продукта.

        Возможный путь — хранить код документации в одном репозитории с кодом продукта.

    ..  revealjs::   Корректность и полнота

        Покрытие (термин из области тестирования) — доля программного кода или «маршрутов» алгоритма, для которой есть тесты.

        Есть инструменты, позволяющие оценить покрытие кода документацией.

        С остальной документацией всё как раньше: её должны проверять эксперты предметной области и наши коллеги-писатели.


..  revealjs:: С чего начать в понедельник

        *   Напишите «Привет, мир!» в онлайн-редакторе: http://dillinger.io/

        *   Посмотрите на различные готовые решения: насколько вам нравится эта документация, удобно ли её читать.

        *   Что используют ваши разработчики?

            *   Система контроля версий
            *   Сервер CI
            *   Внутренняя документация (архитектурная, в коде, тестовая)




..  revealjs:: Вопросы

    nick.volynkin@gmail.com

    Презентация:

    `nick.volynkin.gitlab.io/itgm10`_

    Код этой презентации:

    `gitlab.com/nick.volynkin/itgm10/tree/master`_

    ..  _gitlab.com/nick.volynkin/itgm10/tree/master: https://gitlab.com/nick.volynkin/itgm10/tree/master

